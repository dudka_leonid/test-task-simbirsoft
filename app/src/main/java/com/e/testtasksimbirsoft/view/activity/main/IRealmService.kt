package com.e.testtasksimbirsoft.view.activity.main

import com.e.testtasksimbirsoft.service.realm.DayEventRealmService

interface IRealmService {
    var realmService: DayEventRealmService
}
