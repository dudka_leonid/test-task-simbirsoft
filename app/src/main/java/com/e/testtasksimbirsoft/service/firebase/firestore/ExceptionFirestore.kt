package com.e.testtasksimbirsoft.service.firebase.firestore

import com.e.testtasksimbirsoft.service.ServiceException
import java.lang.Exception

/*
* author Leonid
*/
open class ExceptionFirestore(message: String, e: Exception): ServiceException(message, e)