/*
 * author dudka.leonid@gmail.com
 */

package com.e.testtasksimbirsoft.service

import java.lang.Exception

/**
 * [ServiceException] - класс для того, чтобы отличать ошибки классические от своих кастомных
 * @param message содержит сообщение ошибки
 * @param e хранит в себе любую ошибку класса [Exception]
 *
* @author dudka.leonid@gmail.com
*/
open class ServiceException(val message: String, val e: Exception)