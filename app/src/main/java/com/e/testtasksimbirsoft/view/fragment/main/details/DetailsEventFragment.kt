package com.e.testtasksimbirsoft.view.fragment.main.details

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.ConfigurationCompat

import com.e.testtasksimbirsoft.R
import com.e.testtasksimbirsoft.model.DayEvent
import com.e.testtasksimbirsoft.service.other.showSnackbar
import com.e.testtasksimbirsoft.view.activity.main.IMainActivityNav
import com.e.testtasksimbirsoft.view.activity.main.IRealmService
import java.text.SimpleDateFormat

/**
 * A simple [Fragment] subclass.
 */
class DetailsEventFragment : Fragment() {

    companion object{
        const val TAG_NAME = "DetailsEventFragment"
    }

    lateinit var navigation: IMainActivityNav

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details_event, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        navigation = context as IMainActivityNav

        val btnMain = view.findViewById<Button>(R.id.main_back_button_id)
        val btnEdit = view.findViewById<Button>(R.id.edit_event_button_id)
        val btnDelete = view.findViewById<Button>(R.id.delete_event_button_id)

        val nameEventTextView:TextView = view.findViewById(R.id.name_event_text_view_id)
        val displayDateTextView:TextView = view.findViewById(R.id.display_date_event_text_view_id)
        val description: TextView = view.findViewById(R.id.description_text_view_id)
        val imageView: ImageView = view.findViewById(R.id.image_event_image_view_id)

        val dayEvent: DayEvent = arguments!!.getSerializable("dayEvent") as DayEvent

        val formatDate = SimpleDateFormat("d MMM yyyy",
            ConfigurationCompat.getLocales(resources.configuration).get(0))
        val formatTime = SimpleDateFormat("HH:mm",
            ConfigurationCompat.getLocales(resources.configuration).get(0))

        nameEventTextView.text = dayEvent.name
        displayDateTextView.text =
            "${formatTime.format(dayEvent.date_start)} -" +
                    " ${formatTime.format(dayEvent.date_finish)}    " +
                    formatDate.format(dayEvent.date_start)
        description.text = dayEvent.description

//        Picasso.get().load(Uri.parse(dayEvent.imageURL)).into(imageView)

        btnMain.setOnClickListener {
            navigation.openMain(TAG_NAME)
        }

        btnDelete.setOnClickListener {
            (context as IRealmService).realmService.deleteData(dayEvent)
            val message = "Событие удалено"
            showSnackbar(message, view, context!!)
            navigation.openMain(TAG_NAME)
        }

        btnEdit.setOnClickListener {
            navigation.openEdit(dayEvent)
        }
    }


}
