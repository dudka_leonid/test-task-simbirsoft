package com.e.testtasksimbirsoft.view.custom.calendar

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.widget.*
import androidx.core.os.ConfigurationCompat.getLocales
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.e.testtasksimbirsoft.R
import com.e.testtasksimbirsoft.model.DayEvent
import com.e.testtasksimbirsoft.view.fragment.main.main.MainFragment
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/*
* author Leonid
*/

/** задумка такова и больше никакова: есть календарь [CustomCalendar],
 *  который имеет внутри себя несколько важных элементов:
 *  кнопки переключения месяца [btnNext] и [btnPrev] и возврата на "сегодня",
 *  текстового поля, оторажающего выбранную дату [txtDisplayDate]
 *  и двух элементов [RecyclerView]:
 *  [dayOfWeekGridView] - дни недели и
 *  [dayOfMonthGridView] - дни месяца, ну календарик, ну ведь классно.
 *
 */
class CustomCalendar(private val mContext: Context, attrs: AttributeSet)
    : LinearLayout(mContext, attrs) {

    companion object {
        // Переменная для дебагинга
        const val TAG = "CustomCalendar_TAG_DEBUG_w652387"
        const val DAY_IN_WEEK = 7
        const val WEEKS_COUNT = 6
        const val DAYS_COUNT = 42
        val TITLES_DAY_OF_WEEK =
            arrayListOf("ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ", "ВС")
    }

    // UI компоненты
    private lateinit var btnToday: Button
    private lateinit var btnPrev: Button
    private lateinit var btnNext: Button
    private lateinit var txtDisplayDate: TextView
    private lateinit var dayOfMonthGridView: RecyclerView
    private lateinit var dayOfWeekGridView: RecyclerView

    // Вспомогательные компоненты
    // текущая дата, выбранная пользователем в календаре
    private var currentlyCalendar: Calendar = Calendar.getInstance()

    // текущая дата, выбранныя системой. НЕ МЕНЯЕТСЯ
    private val todayCalendar: Calendar = currentlyCalendar.clone() as Calendar

    // текущие события в календаре
    var events: ArrayList<DayEvent> = ArrayList()

    // событие, срабатывающее на смену дня
    private lateinit var listener: IChangeDayListener

    // адаптер событий (от куда получаем ивенты)
    private var adapterEvents: MainFragment? = null

    // дни в текущем месяце
    private var days: ArrayList<Long> = ArrayList()

    // адаптер календаря с днями месяца
    private var customCalendarAdapter: CustomCalendarAdapter? = null

    //
    private var ccis = ArrayList<CustomCalendarItem>()

    //ПЕременная говно
    private var currentMonthCalendar: Int? = null

    init {
        initControl(context, attrs)
    }

    // ОСНОВНОЕ
    //----------------------------------------------------------------------------------------------
    // инициализация всех компонентов (первый запуск)
    private fun initControl(context: Context, attrs: AttributeSet) {
        val inflater: LayoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.custom_calendar, this)
    }

    // присваиваю значения вьюшкам
    private fun assignUiElements() {
        btnPrev = findViewById(R.id.prev_button_calendar_id)
        btnNext = findViewById(R.id.next_button_calendar_id)
        txtDisplayDate = findViewById(R.id.date_display_id)
        btnToday = findViewById(R.id.date_display_today_id)
        dayOfMonthGridView = findViewById(R.id.calendar_grid_days_of_month_id)
        dayOfWeekGridView = findViewById(R.id.calendar_grid_days_of_week_id)
    }

    // Прикрепляю события к кнопкам
    private fun initButtonEvents() {
        btnPrev.setOnClickListener {
            if (currentMonthCalendar == currentlyCalendar.get(Calendar.MONTH)) {
                currentlyCalendar.add(Calendar.MONTH, -1)
            }
            printDate()
            update()
            customCalendarAdapter!!.selectedDay(currentlyCalendar.get(Calendar.DAY_OF_MONTH))
        }
        btnNext.setOnClickListener {
            if (currentMonthCalendar == currentlyCalendar.get(Calendar.MONTH)) {
                currentlyCalendar.add(Calendar.MONTH, 1)
            }
            printDate()
            update()
            customCalendarAdapter!!.selectedDay(currentlyCalendar.get(Calendar.DAY_OF_MONTH))
        }
        btnToday.setOnClickListener {
            currentlyCalendar.time = this.todayCalendar.time
            printDate()
            update()
            customCalendarAdapter!!.selectedDay(currentlyCalendar.get(Calendar.DAY_OF_MONTH))
        }
    }

    // вывожу в список дней недели названия ДНЕЙ НЕДЕЛИ
    private fun printDayOfWeek() {
        dayOfWeekGridView.layoutManager = GridLayoutManager(mContext, DAY_IN_WEEK)
        val adapter = CustomCalendarAdapter(
            mContext,
            daysOfWeekConvertToCustomCalendarItem(TITLES_DAY_OF_WEEK)
        )
        dayOfWeekGridView.adapter = adapter
    }

    //----------------------------------------------------------------------------------------------
    // ТО, ЧТО НЕ СОВСЕМ ОБЯЗАТЕЬНО, НО БЕЗ ЭТОГО БУДЕТ ЖОПКА))))
    //----------------------------------------------------------------------------------------------
    //
    fun initView() {
        assignUiElements()
        initButtonEvents()
        printDayOfWeek()
        initCalendarDays()
        customCalendarAdapter!!.selectedDay(todayCalendar.get(Calendar.DAY_OF_MONTH))
    }

    // устанавливает листенер
    fun setOnChangeDayListener(listener: IChangeDayListener) {
        this.listener = listener
    }

    // Срабатывает каждый раз, как нажно обновить UI
    fun update() {
        // очищаем список событий
        this.events.clear()
        // заполняем новыми данными
        this.events.addAll(adapterEvents?.findEventsInMonth(currentlyCalendar)!!)

        // очищаем список дней
        this.days.clear()
        // заполняем новыми данными
        this.days.addAll(generateDays())

        // вывожу на экран всё что я нашкодил
        showCustomCalendarItems()
    }

    // задаём новый адаптер событий (от куда получаем список событий)
    fun setAdapterEvents(fragment: MainFragment) {
        this.adapterEvents = fragment
    }

    //----------------------------------------------------------------------------------------------
    // Я НЕ ЗНАЮ ЧТО Я ТУТ ДЕЛАЛ.....
    //----------------------------------------------------------------------------------------------
    // вывожу дату на экран
    private fun printDate() {
        val firstPattern = "d MMM yyyy"
        val primaryLocale = getLocales(resources.configuration).get(0)
        val format = SimpleDateFormat(firstPattern, primaryLocale)
        txtDisplayDate.text = format.format(currentlyCalendar.time)
    }

    // генерирует дни, относительно текущего месяца
    private fun generateDays(): Collection<Long> {
        // Отматываем до ближайшего понедельника
        val newCalendar = rewindDateCalendar(currentlyCalendar)
        // Необходимые переменные для установки дат в массив
        return fillDaysCalendar(newCalendar)
    }

    // отматывает дату, для удобной генерации
    private fun rewindDateCalendar(calendar: Calendar): Calendar {
        val cal = calendar.clone() as Calendar
        cal.set(Calendar.DAY_OF_MONTH, 1)
        while (!isMonday(cal)) {
            cal.add(Calendar.DAY_OF_MONTH, -1)
        }
        return cal
    }

    // проверяет понедельник ли или нет
    private fun isMonday(calendar: Calendar) =
        calendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY

    // возвращает список дат для календаря текущего месяца
    private fun fillDaysCalendar(cal: Calendar): Collection<Long> {
        val days: ArrayList<Long> = ArrayList()
        for (i in 0 until DAYS_COUNT) {
            days.add(cal.time.time)
            cal.add(Calendar.DAY_OF_MONTH, 1)
        }
        return days
    }

    // вывод в таблицу кастомных итемов
    private fun showCustomCalendarItems() {
        this.ccis.clear()
        this.ccis.addAll(daysOfMonthConvertToCustomCalendarItem())
        customCalendarAdapter!!.notifyDataSetChanged()
        currentMonthCalendar = currentlyCalendar.get(Calendar.MONTH)
    }

    private fun initCalendarDays() {
        dayOfMonthGridView.layoutManager = GridLayoutManager(mContext, DAY_IN_WEEK)
        // очищаем список событий
        this.events.clear()
        // заполняем новыми данными
        this.events.addAll(adapterEvents?.findEventsInMonth(currentlyCalendar)!!)

        // очищаем список дней
        this.days.clear()
        // заполняем новыми данными
        this.days.addAll(generateDays())

        this.ccis.clear()
        this.ccis.addAll(daysOfMonthConvertToCustomCalendarItem())

        customCalendarAdapter = CustomCalendarAdapter(mContext, this.ccis)
        // подключаю слушателя на смену дня недели по клику пользователя
        customCalendarAdapter!!
            .setClickListener(CustomCalendarAdapter.ItemClickListener { newVH, oldVH ->
                newVH.txtDay.setTextColor(resources.getColor(R.color.green, context.theme))
                oldVH?.txtDay?.setTextColor(oldVH.item.color)
                this@CustomCalendar.currentlyCalendar.time = Date(newVH.item.date)
                listener.onChange(newVH.item.events)
                printDate()
            })

        dayOfMonthGridView.adapter = customCalendarAdapter

        customCalendarAdapter!!.selectedDay(currentlyCalendar.get(Calendar.DAY_OF_MONTH))
    }

    //
    private fun daysOfWeekConvertToCustomCalendarItem(titlesDayOfWeek: ArrayList<String>)
            : ArrayList<CustomCalendarItem> {
        val color = resources.getColor(R.color.dayOfWeekTextColor, mContext.theme)
        val result = ArrayList<CustomCalendarItem>()
        titlesDayOfWeek.forEach {title ->
            result.add(CustomCalendarItem(title, color, 16F))
        }
        return result
    }

    //
    private fun daysOfMonthConvertToCustomCalendarItem(): ArrayList<CustomCalendarItem> {
        val result = ArrayList<CustomCalendarItem>()
        val calendar = Calendar.getInstance()
            days.forEach { day ->
                calendar.time = Date(day)
                val newEvents =
                    adapterEvents?.findValidEvents(calendar, events)!!
                val color = if (newEvents.isNotEmpty()) {
                    resources.getColor(R.color.colorPrimaryDark, context.theme)
                } else {
                    resources.getColor(R.color.black, mContext.theme)
                }
                result.add(CustomCalendarItem(
                    calendar.get(Calendar.DAY_OF_MONTH).toString(),
                    color,
                    14F,
                    day,
                    newEvents
                ))
            }
        return result
    }
    //----------------------------------------------------------------------------------------------
}