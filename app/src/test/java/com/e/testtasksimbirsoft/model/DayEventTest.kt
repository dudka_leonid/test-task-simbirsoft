package com.e.testtasksimbirsoft.model

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

class DayEventTest{
    @Test
    fun firstInit() {
        val event = DayEvent()
        assertNull(event.id)
        assertNull(event.date_start)
        assertNull(event.date_finish)
        assertNull(event.name)
        assertNull(event.description)
    }

    @Test
    fun secondInit() {
        var calendar = Calendar.getInstance()

        calendar.set(Calendar.YEAR, 2019)
        calendar.set(Calendar.MONTH, 3)
        calendar.set(Calendar.DAY_OF_MONTH, 12)
        calendar.set(Calendar.HOUR_OF_DAY, 14)
        calendar.set(Calendar.MINUTE, 58)
        val startDate = calendar.time

        calendar.set(Calendar.YEAR, 2019)
        calendar.set(Calendar.MONTH, 5)
        calendar.set(Calendar.DAY_OF_MONTH, 3)
        calendar.set(Calendar.HOUR_OF_DAY, 11)
        calendar.set(Calendar.MINUTE, 14)
        val endDate = calendar.time

        val params: HashMap<String, String> =
            hashMapOf(
                "id" to 271460214.toString(),
                "date_start" to startDate.time.toString(),
                "date_finish" to endDate.time.toString(),
                "name" to "asd",
                "description" to "asd"
            )

        val event = DayEvent(
            params["id"]!!.toInt(),
            params["date_start"]!!.toLong(),
            params["date_finish"]!!.toLong(),
            params["name"]!!.toString(),
            params["description"]!!.toString()
        )
        assertEquals(event.id, params["id"]!!.toInt())
        assertEquals(event.date_start, params["date_start"]!!.toLong())
        assertEquals(event.date_finish, params["date_finish"]!!.toLong())
        assertEquals(event.name, params["name"]!!.toString())
        assertEquals(event.description, params["description"]!!.toString())

        val pattern = "yyyy-MM-dd HH:mm:ss"

        calendar = Calendar.getInstance()
        calendar.timeInMillis = event.date_start!!

        println(calendar.time.toString(pattern))
    }

    private infix fun Date.toString(pattern: String): String {
        val simpleDateFormat = SimpleDateFormat(pattern)
        return simpleDateFormat.format(this.time)
    }
}