package com.e.testtasksimbirsoft.service.firebase.auth

import com.e.testtasksimbirsoft.service.ServiceException


open class ExceptionAuth(message: String, e: Exception): ServiceException(message, e)
