/*
 * author dudka.leonid@gmail.com
 */

package com.e.testtasksimbirsoft.service.exception

import com.e.testtasksimbirsoft.service.ServiceException
import java.lang.Exception

/**
 * [InvalidArgument] - ошибка, когда значение, 
 * передаваемое в текстовое поле не сответствует заданному системой требованию.
 * Наследуется от [ServiceException]
 */
class InvalidArgument(message: String, e: Exception) : ServiceException(message, e)
