package com.e.testtasksimbirsoft.view.activity.auth

interface IAuthActivityNav {
    fun openLogin()
    fun openRegister()
    fun openMainActivity()
}
