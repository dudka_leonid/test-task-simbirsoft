package com.e.testtasksimbirsoft.service.datajson

import com.e.testtasksimbirsoft.model.DayEvent
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

/**
 * [JSONDayEvents] - класс для работы с JSON данными класса [DayEvent]
 * Реализует интерфейс [IJSONDayEventsManager]
 * 
 * @author dudka.leonid@gmail.com
 */
class JSONDayEvents: IJSONDayEventsManager {

    override fun fromJson(json: String): List<DayEvent> {
        val collectionType: Type =
            object : TypeToken<List<DayEvent>>() {}.type
        return Gson().fromJson(json, collectionType)
    }

    override fun toJson(data: List<DayEvent>): String {
        return Gson().toJson(data)
    }
}