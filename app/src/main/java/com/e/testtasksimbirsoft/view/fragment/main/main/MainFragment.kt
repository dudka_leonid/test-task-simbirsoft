package com.e.testtasksimbirsoft.view.fragment.main.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.e.testtasksimbirsoft.R
import com.e.testtasksimbirsoft.model.DayEvent
import com.e.testtasksimbirsoft.service.adapter.DayEventsRecyclerAdapter
import com.e.testtasksimbirsoft.service.realm.DayEventRealmService
import com.e.testtasksimbirsoft.view.activity.main.IMainActivityNav
import com.e.testtasksimbirsoft.view.activity.main.IRealmService
import com.e.testtasksimbirsoft.view.activity.main.MainActivity
import com.e.testtasksimbirsoft.view.custom.calendar.CustomCalendar
import com.e.testtasksimbirsoft.view.custom.calendar.IChangeDayListener
import com.e.testtasksimbirsoft.view.custom.calendar.IOpenDetailsEventListener
import com.e.testtasksimbirsoft.view.fragment.main.create.CreateEventCalenderFragment
import com.e.testtasksimbirsoft.view.fragment.main.edit.EditEventFragment
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class MainFragment : Fragment() {

    lateinit var navigation: IMainActivityNav
    lateinit var recyclerDayEvents: RecyclerView
    lateinit var dayEventsAdapter: DayEventsRecyclerAdapter
    lateinit var service: IRealmService

    // Все события в текущем потоке
    private val events: ArrayList<DayEvent> = ArrayList()
    // События текущего дня (Который выбран в календаре)
    private val currentlyEventsOfDay: ArrayList<DayEvent> = ArrayList()

    // Событие срабатывает при нажатии на кнопку "подробнее", расположенную в итеме адаптера
    private lateinit var onClickDayCustomCalendarListener: IOpenDetailsEventListener

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        navigation = context as IMainActivityNav
        service = context as IRealmService

        (context as MainActivity).setEditListener(EditEventFragment.Listener {

        })

        // обработчик нажатий на копку "подробнее" списка событий
        onClickDayCustomCalendarListener = IOpenDetailsEventListener {
            navigation.openDetails(it)
        }
        recyclerDayEvents = view.findViewById(R.id.recycler_day_events_id)
        val calendarView: CustomCalendar = view.findViewById(R.id.customCalendar)
        calendarView.setAdapterEvents(this)
        calendarView.initView()

        // Вешаем обработчик событий на действие "при смене дня в календаре"
        // В ответ получаем дату выбранного дня
        calendarView.setOnChangeDayListener(IChangeDayListener { events ->
            // очищаем прошлые события в списке
            currentlyEventsOfDay.clear()
            currentlyEventsOfDay.addAll(events)
            dayEventsAdapter.notifyDataSetChanged()
        })

        // при первом запуске считываем данные с устройства и инициализируем адаптер
        // последний крепится к списку событий
        service.realmService.readData(DayEventRealmService.ResultListener{
            dayEventsAdapter = DayEventsRecyclerAdapter(
                context!!, currentlyEventsOfDay,
                onClickDayCustomCalendarListener
            )
            val lm = LinearLayoutManager(context!!)
            lm.orientation = LinearLayoutManager.VERTICAL
            recyclerDayEvents.layoutManager = lm
            recyclerDayEvents.adapter = dayEventsAdapter
            events.clear()
            events.addAll(it)
            currentlyEventsOfDay.clear()
            dayEventsAdapter.notifyDataSetChanged()
            calendarView.update()
        })

        // листенер, срабатывающий при УСПЕШНОМ создании нового события
        val createdListener = CreateEventCalenderFragment.CreatedListener { event ->
            // сохраняем новое событие в БД
            service.realmService.saveData(event)
            // добавляем в список всех событий (внутри класса)
            events.add(event)
            // обновляем календарь
            calendarView.update()
        }

        val btnCreate: Button = view.findViewById(R.id.create_button_id)
        btnCreate.setOnClickListener {
            navigation.openCreate(createdListener)
        }
    }

    // Метод по поиску событий. которые находятся в текущем дне [calendar]
    fun findValidEvents(calendar: Calendar, events: ArrayList<DayEvent>): Collection<DayEvent> {
        val startSearchDate = calendar.clone() as Calendar
        val endSearchDate = calendar.clone() as Calendar

        startSearchDate.set(Calendar.HOUR_OF_DAY, 0)
        startSearchDate.set(Calendar.MINUTE, 0)

        endSearchDate.set(Calendar.HOUR_OF_DAY, 23)
        endSearchDate.set(Calendar.MINUTE, 59)

        val x = events.filter {
            it.date_start!! >= startSearchDate.time.time &&
                    it.date_finish!! <= endSearchDate.time.time
        }

        return x
    }

    // Метод по поиску событий. которые находятся в текущем месяце [calendar]
    fun findEventsInMonth(calendar: Calendar): Collection<DayEvent> {
        val startSearchDate = calendar.clone() as Calendar
        val endSearchDate = calendar.clone() as Calendar

        startSearchDate.set(Calendar.HOUR_OF_DAY, 0)
        startSearchDate.set(Calendar.MINUTE, 0)
        startSearchDate.set(Calendar.DAY_OF_MONTH, 1)

        endSearchDate.set(Calendar.HOUR_OF_DAY, 23)
        endSearchDate.set(Calendar.MINUTE, 59)
        endSearchDate.set(Calendar.DAY_OF_MONTH, 1)
        endSearchDate.add(Calendar.MONTH, 1)
        endSearchDate.add(Calendar.DAY_OF_MONTH, -1)

        val x  = events.filter {
            it.date_start!! >= startSearchDate.time.time &&
                    it.date_finish!! <= endSearchDate.time.time
        }
        return x
    }
}
