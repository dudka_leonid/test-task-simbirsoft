package com.e.testtasksimbirsoft.service.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.e.testtasksimbirsoft.R
import com.e.testtasksimbirsoft.model.DayEvent
import com.e.testtasksimbirsoft.view.custom.calendar.IOpenDetailsEventListener

/**
 * [DayEventsRecyclerAdapter] -
 * @param context контекст класса (ГЕНИАЛЬНО, ЗНАЮ)
 * @param events список событий, отображаемых в списке
 * @param listener сушатель, срабатывающий при нажатии на кнопку [ViewHolder.btnDetails]
 */
class DayEventsRecyclerAdapter(
    context: Context,
    private val events: ArrayList<DayEvent>,
    private var listener: IOpenDetailsEventListener
) :
    RecyclerView.Adapter<DayEventsRecyclerAdapter.ViewHolder>() {

    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        /**
         * кнопка, исполняющая [listener] класса [IOpenDetailsEventListener]
         */
        private val btnDetails: Button = view.findViewById(R.id.details_button_id)

        /**
         * текстовое поле, выводящее на экран название события класса [DayEvent]
         */
        private val textEvent: TextView = view.findViewById(R.id.text_event_id)

        fun bind(dayEvent: DayEvent) {
            textEvent.text = dayEvent.name
            btnDetails.setOnClickListener { listener.onClick(dayEvent) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = mInflater.inflate(R.layout.item_event_custom_calendar,
                                                      parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return events.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(events[position])
    }

    fun setOpenDetailsEventListener(listener: IOpenDetailsEventListener) {
        this.listener = listener
    }
}
