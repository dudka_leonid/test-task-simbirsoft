package com.e.testtasksimbirsoft.view.fragment.auth.register

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText

import com.e.testtasksimbirsoft.R
import com.e.testtasksimbirsoft.model.UserData
import com.e.testtasksimbirsoft.service.firebase.FireBaseApi
import com.e.testtasksimbirsoft.service.firebase.auth.ExceptionAuth
import com.e.testtasksimbirsoft.service.firebase.firestore.ExceptionFirestore
import com.e.testtasksimbirsoft.service.other.isValid
import com.e.testtasksimbirsoft.service.other.showSnackbar
import com.e.testtasksimbirsoft.view.activity.auth.IAuthActivityNav
import com.e.testtasksimbirsoft.service.exception.InvalidArgument
import kotlinx.android.synthetic.main.fragment_register.*

/**
 * A simple [Fragment] subclass.
 */
class RegisterFragment : Fragment() {

    lateinit var navigation: IAuthActivityNav
    lateinit var authService: FireBaseApi.FireBaseAuth
    lateinit var firestoreService: FireBaseApi.FireBaseFirestore


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        navigation = context as IAuthActivityNav
        val btnRegister = view.findViewById<Button>(R.id.register_button_id)
        val usernameEditText: EditText = view.findViewById(R.id.edit_text_username_id)
        val loginEditText: EditText = view.findViewById(R.id.edit_text_login_id)
        val passwordEditText: EditText = view.findViewById(R.id.edit_text_password_id)
        authService = FireBaseApi.FireBaseAuth()
        firestoreService = FireBaseApi.FireBaseFirestore()
        btnRegister.setOnClickListener {
            val username = usernameEditText.text.toString()
            val login = loginEditText.text.toString()
            val password = passwordEditText.text.toString()
            if (!isValid(login) || !isValid(password) || !isValid(username)) {
                showSnackbar(
                    InvalidArgument(
                        "Проверьте правильность введённых данных",
                        IllegalArgumentException()
                    ),
                    root_layout_register_fragment,
                    context!!)
            } else {
                authService.singUp(login, password, object : FireBaseApi.FireBaseAuth.Listener {
                    override fun onLoading() {

                    }

                    override fun onSuccessful() {
                        firestoreService.saveData(UserData(authService.getUser()!!.uid, username),
                            object : FireBaseApi.FireBaseFirestore.Listener {
                                override fun onLoading() {

                                }

                                override fun onSuccessful(userData: UserData?) {
                                    navigation.openMainActivity()
                                }

                                override fun onError(e: ExceptionFirestore) {
                                    showSnackbar(e, root_layout_register_fragment, context!!)
                                }
                            })
                    }

                    override fun onError(e: ExceptionAuth) {
                        showSnackbar(e, root_layout_register_fragment, context!!)
                    }
                })
            }
        }
    }


}
