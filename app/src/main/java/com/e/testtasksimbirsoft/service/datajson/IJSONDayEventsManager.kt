package com.e.testtasksimbirsoft.service.datajson

import com.e.testtasksimbirsoft.model.DayEvent

/**
 * [IJSONDayEventsManager] - содержит набор методов для любого класса, 
 * работающий с конвертором json -> object, object -> json и тому подобные.
 * 
 * @author dudka.leonid@gmail.com
 */
interface IJSONDayEventsManager {
    /**
     * json -> List<DayEvent>
     */
    fun fromJson(json: String): List<DayEvent>

    /**
     * List<DayEvent> -> json
     */
    fun toJson(data: List<DayEvent>): String
}
