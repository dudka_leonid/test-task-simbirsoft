package com.e.testtasksimbirsoft.service.firebase.firestore

import com.e.testtasksimbirsoft.model.UserData
import com.e.testtasksimbirsoft.service.firebase.FireBaseApi

interface IFireBaseFirestore {
    fun saveData(userData: UserData, listener: FireBaseApi.FireBaseFirestore.Listener)
    fun loadData(uid: String, listener: FireBaseApi.FireBaseFirestore.Listener)
}
