package com.e.testtasksimbirsoft.view.activity.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.e.testtasksimbirsoft.R
import com.e.testtasksimbirsoft.view.activity.main.MainActivity

class AuthActivity : AppCompatActivity(),
    IAuthActivityNav {

    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth2)

        navController = Navigation.findNavController(this, R.id.auth_container_id)

    }

    override fun openLogin() {
        navController.popBackStack(R.id.loginFragment, true)
        navController.popBackStack(R.id.registerFragment, true)
        navController.navigate(R.id.loginFragment)
    }

    override fun openRegister() {
        navController.popBackStack(R.id.registerFragment, true)
        navController.navigate(R.id.registerFragment)
    }

    override fun openMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}
