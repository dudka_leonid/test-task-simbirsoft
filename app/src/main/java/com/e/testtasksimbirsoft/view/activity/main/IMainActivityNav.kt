package com.e.testtasksimbirsoft.view.activity.main

import com.e.testtasksimbirsoft.model.DayEvent
import com.e.testtasksimbirsoft.view.fragment.main.create.CreateEventCalenderFragment

interface IMainActivityNav {
    fun openMain(tagName: String)
    fun openDetails(dayEvent: DayEvent)
    fun openCreate(
        listener: CreateEventCalenderFragment.CreatedListener
    )
    fun openEdit(dayEvent: DayEvent)
}
