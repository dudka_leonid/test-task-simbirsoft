package com.e.testtasksimbirsoft.service.datajson

import com.e.testtasksimbirsoft.model.DayEvent
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import java.util.*
import kotlin.collections.ArrayList

class JSONDayEventsTest {

    lateinit var service: JSONDayEvents
    lateinit var collection: ArrayList<DayEvent>

    @Before
    fun init() {
        service = JSONDayEvents()
        collection = arrayListOf(
            DayEvent(
                1,
                getDate(2019, 1, 12, 15, 32, 45),
                getDate(2019, 2, 13, 12, 54, 8),
                "Первое событие",
                "Ну тут надо что-то написать, значит нужно 1"
            ),
            DayEvent(
                2,
                getDate(2029, 3, 14, 15, 32, 45),
                getDate(2029, 4, 15, 12, 54, 8),
                "Второе событие",
                "Ну тут надо что-то написать, значит нужно 2"
            ),
            DayEvent(
                3,
                getDate(2039, 5, 16, 15, 32, 45),
                getDate(2039, 6, 17, 12, 54, 8),
                "Второе событие",
                "Ну тут надо что-то написать, значит нужно 3"
            )
        )
    }

    private fun getDate(
        year: Int,  month: Int,  date: Int,  hourOfDay: Int,  minute: Int, second: Int
    ): Long? {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, date, hourOfDay, minute, second)
        return calendar.time.time
    }

    @Test
    fun firstTest() {
        val dayEventsJSON: String = service.toJson(collection)
        println(dayEventsJSON)
        val dayEvents: List<DayEvent> = service.fromJson(dayEventsJSON)
        dayEvents.forEach {
            println(it)
        }
    }
}