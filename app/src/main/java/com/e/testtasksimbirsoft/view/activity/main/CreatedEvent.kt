package com.e.testtasksimbirsoft.view.activity.main

import com.e.testtasksimbirsoft.model.DayEvent

/*
* author Leonid
*/
fun interface CreatedEvent {
    fun onCreated(dayEvent: DayEvent)
}