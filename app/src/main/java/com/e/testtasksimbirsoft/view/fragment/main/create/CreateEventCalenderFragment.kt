package com.e.testtasksimbirsoft.view.fragment.main.create

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.os.ConfigurationCompat

import com.e.testtasksimbirsoft.R
import com.e.testtasksimbirsoft.model.DayEvent
import com.e.testtasksimbirsoft.view.activity.main.CreatedEvent
import com.e.testtasksimbirsoft.view.activity.main.IMainActivityNav
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*
import kotlin.random.Random

/**
 * A simple [Fragment] subclass.
 */
class CreateEventCalenderFragment : Fragment(),
    DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    companion object{
        const val TAG_NAME = "CreateEventCalenderFragment"
    }

    //ToDo поменяй цветовую палитру, язык даты

    lateinit var navigation: IMainActivityNav
    lateinit var formatDate: SimpleDateFormat
    lateinit var calendar: Calendar
    lateinit var startDateTimeEvent: Calendar
    lateinit var endDateTimeEvent: Calendar
    var currentTimeEventId: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_event_calender, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        navigation = context as IMainActivityNav

        calendar = Calendar.getInstance()
        startDateTimeEvent = calendar.clone() as Calendar
        endDateTimeEvent = calendar.clone() as Calendar

        initComponents()

        formatDate = SimpleDateFormat("d MMM yyyy",
            ConfigurationCompat.getLocales(resources.configuration).get(0))

        printDate()

        val btnMain = view.findViewById<Button>(R.id.main_back_button_id)
        btnMain.setOnClickListener {
            navigation.openMain(TAG_NAME)
            createEvent()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun printDate() {
        dateButton.text = formatDate.format(calendar.time)
        startTimeButton.text = calendar.get(Calendar.HOUR_OF_DAY).toString() +
                                ":" +
                                calendar.get(Calendar.MINUTE)
        endTimeButton.text = calendar.get(Calendar.HOUR_OF_DAY).toString() +
                ":" +
                calendar.get(Calendar.MINUTE)
    }

    lateinit var nameEditText: EditText
    lateinit var descriptionEditText: EditText
    lateinit var dateButton: Button
    lateinit var startTimeButton: Button
    lateinit var endTimeButton: Button
    lateinit var imageView: ImageView

    private fun initComponents() {
        nameEditText = view!!.findViewById(R.id.edit_text_name_id)
        descriptionEditText = view!!.findViewById(R.id.edit_text_description_id)
        dateButton = view!!.findViewById(R.id.change_date_button_id)
        startTimeButton = view!!.findViewById(R.id.start_time_button_id)
        endTimeButton = view!!.findViewById(R.id.end_time_button_id)
        imageView = view!!.findViewById(R.id.image_event_image_view_id)

        dateButton.setOnClickListener {
            showDatePicker()
        }

        startTimeButton.setOnClickListener {
            currentTimeEventId = startTimeButton.id
            showTimePicker(startDateTimeEvent)
        }

        endTimeButton.setOnClickListener {
            currentTimeEventId = endTimeButton.id
            showTimePicker(endDateTimeEvent)
        }

        imageView.setOnClickListener{
            galleryIntent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            galleryIntent.type = "image/*"
            startActivityForResult(galleryIntent, 1)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1 && requestCode == Activity.RESULT_OK) {
            imageUri = data!!.data
            Picasso.get().load(imageUri).into(imageView)
        }
    }

    private lateinit var galleryIntent: Intent
    private var imageUri: Uri? = null

    private fun showDatePicker() {
        val dataPickerDialog = DatePickerDialog(
            context!!,
            R.style.DialogBaseStyle,
            this,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        dataPickerDialog.show()
    }

    private fun showTimePicker(time: Calendar) {
        val timePickerDialog = TimePickerDialog(context!!,
            R.style.DialogBaseStyle,this,
            time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE),
            android.text.format.DateFormat.is24HourFormat(context!!))
        timePickerDialog.show()
    }

    private fun createEvent() {
        val dayEvent: DayEvent = DayEvent()

        // Задать id
        dayEvent.id = Random.nextInt(999999999)

        // Задать название
        val nameDayEvent = nameEditText.text.toString()
        dayEvent.name = nameDayEvent

        // Задать описание
        val descriptionDayEvent = descriptionEditText.text.toString()
        dayEvent.description = descriptionDayEvent

        // Задать стартовую дату (дата + время)
        dayEvent.date_start = startDateTimeEvent.time.time

        // Задать конечную дату (дата + время)
        dayEvent.date_finish = endDateTimeEvent.time.time

        dayEvent.imageURL = imageUri.toString()

        (context as CreatedEvent).onCreated(dayEvent)
    }

    fun interface CreatedListener {
       fun onCreate(dayEvent: DayEvent)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        setDate(calendar, year, month, dayOfMonth)
        setDate(startDateTimeEvent, year, month, dayOfMonth)
        setDate(endDateTimeEvent, year, month, dayOfMonth)
        dateButton.text = formatDate.format(calendar.time)
    }

    private fun setDate(calendar: Calendar, year: Int, month: Int, dayOfMonth: Int) {
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
    }

    @SuppressLint("SetTextI18n")
    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        when (currentTimeEventId) {
            startTimeButton.id -> {
                this.startDateTimeEvent.set(Calendar.HOUR_OF_DAY, hourOfDay)
                this.startDateTimeEvent.set(Calendar.MINUTE, minute)
                startTimeButton.text = "$hourOfDay:$minute"
            }
            endTimeButton.id -> {
                this.endDateTimeEvent.set(Calendar.HOUR_OF_DAY, hourOfDay)
                this.endDateTimeEvent.set(Calendar.MINUTE, minute)
                endTimeButton.text = "$hourOfDay:$minute"
            }
        }
    }

}
