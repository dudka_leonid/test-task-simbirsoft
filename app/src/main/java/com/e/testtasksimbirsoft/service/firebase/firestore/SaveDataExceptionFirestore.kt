package com.e.testtasksimbirsoft.service.firebase.firestore

import java.lang.Exception

/*
* author Leonid
*/class SaveDataExceptionFirestore(message: String, e: Exception): ExceptionFirestore(message, e)