package com.e.testtasksimbirsoft.view.fragment.auth.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText

import com.e.testtasksimbirsoft.R
import com.e.testtasksimbirsoft.service.firebase.FireBaseApi
import com.e.testtasksimbirsoft.service.firebase.auth.ExceptionAuth
import com.e.testtasksimbirsoft.service.other.isValid
import com.e.testtasksimbirsoft.service.other.showSnackbar
import com.e.testtasksimbirsoft.view.activity.auth.IAuthActivityNav
import com.e.testtasksimbirsoft.service.exception.InvalidArgument
import kotlinx.android.synthetic.main.fragment_login.*

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment() {

    lateinit var navigation: IAuthActivityNav
    lateinit var service: FireBaseApi.FireBaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        navigation = context as IAuthActivityNav

        val btnLogin: Button = view.findViewById(R.id.login_button_id)
        val btnRegister: Button = view.findViewById(R.id.register_button_id)
        val loginEditText: EditText = view.findViewById(R.id.edit_text_login_id)
        val passwordEditText: EditText = view.findViewById(R.id.edit_text_password_id)
        service = FireBaseApi.FireBaseAuth()

        if (service.getUser() != null) navigation.openMainActivity()

        btnRegister.setOnClickListener { 
            navigation.openRegister()
        }
        btnLogin.setOnClickListener {
            val login = loginEditText.text.toString()
            val password = passwordEditText.text.toString()
            if (!isValid(login) || !isValid(password)) {
                showSnackbar(
                    InvalidArgument(
                        "Проверьте правильность введённых данных",
                        IllegalArgumentException()
                    ),
                    root_layout_login_fragment,
                    context!!)
            } else {
                service.singIn(login, password, object : FireBaseApi.FireBaseAuth.Listener {
                    override fun onLoading() {

                    }

                    override fun onSuccessful() {
                        navigation.openMainActivity()
                    }

                    override fun onError(e: ExceptionAuth) {
                        showSnackbar(e, root_layout_login_fragment, context!!)
                    }
                })
            }
        }
    }
}
