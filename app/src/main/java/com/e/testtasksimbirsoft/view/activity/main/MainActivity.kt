package com.e.testtasksimbirsoft.view.activity.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.e.testtasksimbirsoft.R
import com.e.testtasksimbirsoft.model.DayEvent
import com.e.testtasksimbirsoft.service.other.showSnackbar
import com.e.testtasksimbirsoft.service.realm.DayEventRealmService
import com.e.testtasksimbirsoft.view.fragment.main.create.CreateEventCalenderFragment
import com.e.testtasksimbirsoft.view.fragment.main.details.DetailsEventFragment
import com.e.testtasksimbirsoft.view.fragment.main.edit.EditEventFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),
    IMainActivityNav, CreatedEvent, IRealmService{

    lateinit var navController: NavController
    override lateinit var realmService: DayEventRealmService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navController = Navigation.findNavController(this, R.id.main_container_id)
        realmService = DayEventRealmService(this)

//        //ToDo все что тут ниже используется, ты нифига не закомител. Так же прошлый коммит.....проставь котлин доки
//        val service = DayEventRealmService(this)
//        service.setChangeListener(object : DayEventRealmService.Listener {
//            override fun onChange(response: String) {
//                Log.e("MainActivity_asfjnaskljf", response)
//            }
//        })
//        service.deleteAll()
//        service.saveData(DayEvent())
//        service.saveData(DayEvent(1, name = "asfasfa"))
//        service.saveData(DayEvent(2, name = "adgasdf", description = "sdgbfawsf34223"))
    }

    //ToDo почини это дерьмо
    override fun openMain(tagName: String) {
        when(tagName) {
            DetailsEventFragment.TAG_NAME -> {
                navController.navigate(R.id.action_detailsEventFragment_to_mainFragment)
            }
            CreateEventCalenderFragment.TAG_NAME -> {
                navController.navigate(R.id.action_createEventCalenderFragment_to_mainFragment)
            }
        }
    }

    override fun openDetails(dayEvent: DayEvent) {
        val bundle = Bundle()
        bundle.putSerializable("dayEvent", dayEvent)
        navController.navigate(R.id.action_mainFragment_to_detailsEventFragment, bundle)
    }

    private lateinit var createdListener: CreateEventCalenderFragment.CreatedListener

    override fun openCreate(
        listener: CreateEventCalenderFragment.CreatedListener
    ) {
        this.createdListener = listener
        navController.navigate(R.id.action_mainFragment_to_createEventCalenderFragment)
    }

    override fun onCreated(dayEvent: DayEvent) {
        createdListener.onCreate(dayEvent)
        val message = "Событие создано"
        showSnackbar(message, root_layout_main_activity, this)
    }

    override fun openEdit(dayEvent: DayEvent) {
        val bundle = Bundle()
        bundle.putSerializable("dayEvent", dayEvent)
        navController
            .navigate(R.id.action_detailsEventFragment_to_editEventFragment, bundle)
    }

    private lateinit var editedListener: EditEventFragment.Listener
    fun setEditListener(listener: EditEventFragment.Listener) {
        this.editedListener = listener
    }
}
