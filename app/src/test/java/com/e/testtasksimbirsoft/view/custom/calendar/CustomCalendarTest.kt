package com.e.testtasksimbirsoft.view.custom.calendar

import org.junit.Test
import java.util.*

/*
 * author Leonid
 */
class   CustomCalendarTest {

    @Test
    fun testDateCalendar() {
        val weeksCount = 5
        val dayOFWeek = 7

        // Получаю экземпляр календаря с текущей датой
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MONTH, 1)
        // Создаю массив дат месяца
        val days = Array(weeksCount) { LongArray(dayOFWeek) }
        // Отматываем до ближайшего понедельника
        val cal = rewindDateCalendar(calendar)
        // Необходимые переменные для установки дат в массив
        fillDaysCalendar(dayOFWeek, weeksCount, days, cal)
        //Выводим на экран
        printCalendar(calendar, days)
    }

    private fun rewindDateCalendar(calendar: Calendar): Calendar {
        val cal = calendar.clone() as Calendar
        cal.set(Calendar.DAY_OF_MONTH, 1)
        while (!isMonday(cal)) {
            cal.add(Calendar.DAY_OF_MONTH, -1)
        }
        return cal
    }

    private fun fillDaysCalendar(
        dayOFWeek: Int,
        weeksCount: Int,
        days: Array<LongArray>,
        cal: Calendar
    ) {
        var count = 0
        var weekId = 0
        var weekDays = LongArray(dayOFWeek)
        for (i in 1..dayOFWeek * weeksCount) {
            if (count == 7) {
                days[weekId++] = weekDays
                weekDays = LongArray(7)
                count = 0
            }
            weekDays[count++] = cal.time.time
            cal.add(Calendar.DAY_OF_MONTH, 1)
        }
        days[weekId] = weekDays
    }

    private fun printCalendar(calendar: Calendar, days: Array<LongArray>) {
        println(calendar.time)
        println("Пн\tВт\tСр\tЧт\tПт\tСб\tВс")
        days.forEach { week ->
            week.forEach { day ->
                if (day == 0L) {
                    print("$day,\t")
                } else {
                    calendar.time = Date(day)
                    print("${calendar.get(Calendar.DAY_OF_MONTH)},\t")
                }
            }
            println("!")
        }
    }

    private fun isMonday (calendar: Calendar) =
        calendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY
}