package com.e.testtasksimbirsoft.service.realm

import android.content.Context
import android.util.Log
import com.e.testtasksimbirsoft.model.DayEvent
import io.realm.Realm

/**
 * [DayEventRealmService] - сервис для работы с [Realm] внутри проекта
 * @author dudka.leonid@gmail.com
 *
 * Классу ****, его нужно менять))))))
 */
class DayEventRealmService{

    companion object {
        /**
         * тег класса, в основном используется для логирования и отладки класса
         */
        const val TAG = "RealmService_DEBUT_LOG"
    }

    /**
     * переменная, хранящая в себе экземпляр класса [Realm]
     */
    private val realm: Realm

    /**
     * переменная, хранящая в себе реализацию интерфейса [IChangeListener]
     */
    private var listener: IChangeListener = IChangeListener { }

    constructor(context: Context) {
        Realm.init(context)
        realm = Realm.getDefaultInstance()
    }

    constructor(realm: Realm) {
        this.realm = realm
    }

    fun setChangeListener(listener: IChangeListener) {
        this.listener = listener
    }

    //ToDo Удаляет все данные
    fun deleteAll() {
        realm.beginTransaction()
        realm.deleteAll()
        realm.commitTransaction()
    }

    //Todo допилю потом
    //----------------------------------------------------------------------------------------------
    fun deleteData(data: DayEvent) {
        realm.executeTransactionAsync({
            try {
                val datos =
                    it.where(DayEvent::class.java)
                        .equalTo("id", data.id)
                        .findAll()
                Log.e(TAG, datos.asJSON())
                datos.deleteAllFromRealm()
            } catch (e: Exception) {
                Log.e(TAG, e.toString() + "\n" + e.message)
                throw e
            }
        },{
            Log.d(TAG,"On Success: Data Written Successfully!")
        },{
            Log.d(TAG,"On Error: Error in saving Data!")
        })
    }

    fun changeData(data: DayEvent) {
        realm.executeTransactionAsync {
            val datos =
                it.where(DayEvent::class.java)
                    .equalTo("id", data.id)
                    .findFirst()

            if (datos != null) {
                datos.setData(data)
                realm.insertOrUpdate(datos)
            }
        }
    }
    //----------------------------------------------------------------------------------------------

    //ToDo сохраняет даннее
    fun saveData(data: DayEvent) {
        realm.executeTransactionAsync ({
            val student = it.createObject(DayEvent::class.java)
            student.id = data.id
            student.date_start = data.date_start
            student.date_finish = data.date_finish
            student.name = data.name
            student.description = data.description
        },{
            Log.d(TAG,"On Success: Data Written Successfully!")
        },{
            Log.d(TAG,"On Error: Error in saving Data!")
        })
    }

    //ToDo считывает все данные
    fun readData(listener: ResultListener) {
        val students = realm.where(DayEvent::class.java).findAll()
        val response = ArrayList<DayEvent>()
        students.forEach {
            response.add(it)
        }
        listener.onResult(response)
    }

    fun interface IChangeListener{
        fun onChange(response: String)
    }

    fun interface ResultListener {
        fun onResult(response: ArrayList<DayEvent>)
    }
}