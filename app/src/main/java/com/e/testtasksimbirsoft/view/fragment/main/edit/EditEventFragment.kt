package com.e.testtasksimbirsoft.view.fragment.main.edit

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.e.testtasksimbirsoft.R
import com.e.testtasksimbirsoft.view.activity.main.IMainActivityNav

/**
 * A simple [Fragment] subclass.
 */
class EditEventFragment : Fragment() {

    lateinit var navigation: IMainActivityNav

    companion object{
        const val TAG_NAME = "DetailsEventFragment"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_event_calender, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        navigation = context as IMainActivityNav
        val btnMain = view.findViewById<Button>(R.id.main_back_button_id)
        btnMain.setOnClickListener {
            navigation.openMain(TAG_NAME)
        }
    }

    fun interface Listener {
        fun onEdited()
    }
}
