package com.e.testtasksimbirsoft.service.firebase

import android.net.Uri
import com.e.testtasksimbirsoft.model.UserData
import com.e.testtasksimbirsoft.service.firebase.auth.ExceptionAuth
import com.e.testtasksimbirsoft.service.firebase.auth.IFireBaseAuth
import com.e.testtasksimbirsoft.service.firebase.auth.SingInExceptionAuth
import com.e.testtasksimbirsoft.service.firebase.auth.SingUpExceptionAuth
import com.e.testtasksimbirsoft.service.firebase.firestore.ExceptionFirestore
import com.e.testtasksimbirsoft.service.firebase.firestore.IFireBaseFirestore
import com.e.testtasksimbirsoft.service.firebase.firestore.LoadDataExceptionFirestore
import com.e.testtasksimbirsoft.service.firebase.firestore.SaveDataExceptionFirestore
import com.e.testtasksimbirsoft.service.firebase.storage.IFireBaseStorage
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.io.File


/*
* author Leonid
*/
class FireBaseApi {

    class FireBaseAuth:
        IFireBaseAuth {

        private var auth = FirebaseAuth.getInstance()

        fun getUser(): FirebaseUser? {
            return auth.currentUser
        }

        override fun singIn(login: String, password: String, listener: Listener) {
            listener.onLoading()
            auth.signInWithEmailAndPassword(login, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        listener.onSuccessful()
                    } else {
                        listener.onError(SingInExceptionAuth(
                            "Ошибка авторизации!", task.exception!!))
                    }
                }
        }

        override fun singUp(login: String, password: String, listener: Listener) {
            listener.onLoading()
            auth.createUserWithEmailAndPassword(login, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        listener.onSuccessful()
                    } else {
                        listener.onError(SingUpExceptionAuth(
                            "Ошибка регистрации!", task.exception!!))
                    }
                }
        }

        override fun singOut() {
            auth.signOut()
        }

        interface Listener {
            fun onLoading()
            fun onSuccessful()
            fun onError(e: ExceptionAuth)
        }

    }

    class FireBaseStorage:
        IFireBaseStorage {
        var mStorageRef = FirebaseStorage.getInstance().getReference();
        private val pathUriServer = "images/"
        private val postfix = ".jpg"

        fun addImage(imageUri: Uri, id: String) {
            mStorageRef
                .child(pathUriServer + id + postfix)
                .putFile(imageUri)
        }

        fun getImage(id: String) {
            val localFile: File = File.createTempFile(id, "jpg")

            mStorageRef
                .child(pathUriServer + id + postfix)
                .getFile(localFile)
        }
    }

    class FireBaseFirestore:
        IFireBaseFirestore {
        private val db = FirebaseFirestore.getInstance()
        private val titleUsersCollection = "users"

        override fun saveData(userData: UserData, listener: Listener) {
            listener.onLoading()
            db.collection(titleUsersCollection)
                .document(userData.uid)
                .set(userData)
                .addOnSuccessListener { listener.onSuccessful(null) }
                .addOnFailureListener { listener.onError(SaveDataExceptionFirestore(
                    "Ошибка сохранения данных!", it)
                ) }
        }

        override fun loadData(uid: String, listener: Listener) {
            listener.onLoading()
            db.collection(titleUsersCollection)
                .document(uid)
                .get()
                .addOnSuccessListener {
                    val data = UserData(uid)
                    data.setData(it.data)
                    listener.onSuccessful(data)
                }
                .addOnFailureListener { listener.onError(LoadDataExceptionFirestore(
                    "Ошибка загрузки данных!", it)
                )  }
        }

        interface Listener {
            fun onLoading()
            fun onSuccessful(userData: UserData?)
            fun onError(e: ExceptionFirestore)
        }
    }
}