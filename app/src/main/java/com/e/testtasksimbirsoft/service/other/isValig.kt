package com.e.testtasksimbirsoft.service.other

/**
 * Проверка на "не пустоту"
 * @param arg строковое значение
 * @return истину или лож
 */
fun isValid(arg: String): Boolean {
    return arg.isNotEmpty()
}