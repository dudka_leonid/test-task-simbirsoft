package com.e.testtasksimbirsoft.model

import io.realm.RealmObject
import java.io.Serializable
import java.util.*

/**
 * [DayEvent] - дата-класс, хранящий в себе информацию определённого события в календаре.
 * Наследуется от класса [RealmObject]
 *
 * @param id id события в формате [Int]
 * @param date_start дата начала события в формате [Long]
 * @param date_finish дата завершения события в формате [Long]
 * @param name название события в формате [String]
 * @param description описание события в формате [String]
 * @param imageURL хранит в себе ссылку на изображение
 *
 *@author dudka.leonid@gmail.com
 */
open class DayEvent(
    var id: Int?= null,
    var date_start: Long?= null,
    var date_finish: Long?= null,
    var name: String?= null,
    var description: String?= null,
    var imageURL: String?= null
) : RealmObject(), Serializable {
    override fun toString(): String {
        val builder = StringBuilder()
        builder.append("DayEvent id: $id, ")
        builder.append("name: $name, ")
        builder.append("date: ${Date(date_start!!)} - ${Date(date_finish!!)}, ")
        builder.append("description: $description.")
        builder.append("imageURL: $imageURL")
        return builder.toString()
    }

    fun setData(data: DayEvent) {
        this.id = data.id
        this.date_start = data.date_start
        this.date_finish = data.date_finish
        this.name = data.name
        this.description = data.description
        this.imageURL = data.imageURL
    }
}