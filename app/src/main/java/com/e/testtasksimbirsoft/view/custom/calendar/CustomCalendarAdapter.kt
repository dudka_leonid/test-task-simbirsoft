package com.e.testtasksimbirsoft.view.custom.calendar

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.e.testtasksimbirsoft.R
import com.e.testtasksimbirsoft.view.custom.calendar.CustomCalendar.Companion.TAG
import kotlin.collections.ArrayList

/*
* author Leonid
*/
class CustomCalendarAdapter(val context: Context, private val days: ArrayList<CustomCalendarItem>) :
    RecyclerView.Adapter<CustomCalendarAdapter.ViewHolder>() {

    val calendarDaysVH: ArrayList<ViewHolder> = ArrayList()
    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private var mClickListener: ItemClickListener? = null
    private var selectedDay: ViewHolder? = null
    var onSelectListener: (ViewHolder) -> Unit = {}
    private var isSelected: Boolean = false
    private var selectedDayEvent: String? = null

    inner class ViewHolder(private val view: View): RecyclerView.ViewHolder(view)
    {
        var listener: ItemClickListener? =  null
            private set
        lateinit var item: CustomCalendarItem
            private set
        val txtDay: TextView = view.findViewById(R.id.day_text_view_id)

        fun bind(item: CustomCalendarItem, listener: ItemClickListener?) {
            this.listener = listener
            this.item = item
            txtDay.text = item.text
            txtDay.setTextColor(item.color)
            txtDay.textSize = item.textSize
            txtDay.setOnClickListener {
                listener?.onItemClick(this, selectedDay)
                selectedDay = this
            }
            if(selectedDayEvent != null && selectedDayEvent?.equals(item.text)!!) {
                onSelectListener(this)
                selectedDayEvent = null
            }
        }
    }

    fun interface ItemClickListener {
        fun onItemClick(
            item: ViewHolder,
            oldSelected: ViewHolder?
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = mInflater.inflate(R.layout.custom_day_calendar, parent, false)
        val vh = ViewHolder(view)
        calendarDaysVH.add(vh)
        return vh
    }

    override fun getItemCount(): Int {
        return days.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(days[position], mClickListener)
    }

    fun getItem(id: Int): CustomCalendarItem {
        return days[id]
    }

    fun setClickListener(itemClickListener: ItemClickListener) {
        this.mClickListener = itemClickListener
    }

    fun selectedDay(day: Int) {
        selectedDayEvent = day.toString()
        onSelectListener = {
            it.listener?.onItemClick(it, selectedDay)
            selectedDay = it
        }
    }
}