package com.e.testtasksimbirsoft.service.firebase.firestore

import java.lang.Exception

/*
* author Leonid
*/class LoadDataExceptionFirestore(message: String, e: Exception): ExceptionFirestore(message, e)