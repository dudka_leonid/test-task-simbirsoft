package com.e.testtasksimbirsoft.view.custom.calendar

import com.e.testtasksimbirsoft.model.DayEvent

/*
* author Leonid
*/
data class CustomCalendarItem(
    val text: String = "",
    val color: Int = 0,
    val textSize: Float = 0F,
    val date: Long = 0L,
    val events: Collection<DayEvent> = arrayListOf()
) {
}