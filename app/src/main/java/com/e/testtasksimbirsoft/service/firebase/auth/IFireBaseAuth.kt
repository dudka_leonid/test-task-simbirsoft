package com.e.testtasksimbirsoft.service.firebase.auth

import com.e.testtasksimbirsoft.service.firebase.FireBaseApi

interface IFireBaseAuth {
    fun singIn(login: String, password: String, listener: FireBaseApi.FireBaseAuth.Listener)
    fun singUp(login: String, password: String, listener: FireBaseApi.FireBaseAuth.Listener)
    fun singOut()
}
