package com.e.testtasksimbirsoft.view.custom.calendar

import com.e.testtasksimbirsoft.model.DayEvent

fun interface IOpenDetailsEventListener {
    fun onClick(dayEvent: DayEvent)
}
