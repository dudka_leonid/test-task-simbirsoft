package com.e.testtasksimbirsoft.service.other

import android.content.Context
import android.view.View
import android.widget.TextView
import com.e.testtasksimbirsoft.R
import com.e.testtasksimbirsoft.service.ServiceException
import com.google.android.material.snackbar.Snackbar


/**
 * Отображение сообщения с ошибкой
 */
fun showSnackbar(e: ServiceException, view: View, context: Context) {
    val snackbar = Snackbar
        .make(view, e.message, Snackbar.LENGTH_SHORT)
    val view: View = snackbar.view
    view.setBackgroundColor(context.resources.getColor(R.color.reed, context.theme))
    val textView: TextView = view.findViewById(com.google.android.material.R.id.snackbar_text)
    textView.setTextColor(context.resources.getColor(R.color.white, context.theme))
    snackbar.show()
}

/**
 * Отображение сообщения об успешном событии
 */
fun showSnackbar(text: String, view: View, context: Context) {
    val snackbar = Snackbar
        .make(view, text, Snackbar.LENGTH_SHORT)
    val view: View = snackbar.view
    view.setBackgroundColor(context.resources.getColor(R.color.green, context.theme))
    val textView: TextView = view.findViewById(com.google.android.material.R.id.snackbar_text)
    textView.setTextColor(context.resources.getColor(R.color.white, context.theme))
    snackbar.show()
}