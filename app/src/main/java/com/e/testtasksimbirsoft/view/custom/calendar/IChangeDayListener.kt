package com.e.testtasksimbirsoft.view.custom.calendar

import com.e.testtasksimbirsoft.model.DayEvent

/*
* author Leonid
*/fun interface IChangeDayListener {
    fun onChange(date: Collection<DayEvent>)
}