package com.e.testtasksimbirsoft.model

/**
 * [UserData] - класс для транзакций между устройством и сервером Firebase
 * @param uid главный ключ сущьности
 * @param username имя пользователя
 * @param events список событий пользователя
 *
 *@author dudka.leonid@gmail.com
*/
data class UserData(val uid: String = "", var username: String  = "", var events: ArrayList<DayEvent> = arrayListOf()) {

    /**
     * Задаёт значения сущьности [data] в текущую сущьность
     */
    fun setData(data: Map<String, Any>?) {
        this.username = data!!["username"].toString()
        this.events = data["events"] as ArrayList<DayEvent>
    }
}